import firebase from 'firebase';

const config = {
  // apiKey: 'AIzaSyDvacomox9JzzzR6qTYGZJhS15elVfs6NQ',
  // authDomain: 'vydaje-843c0.firebaseapp.com',
  // databaseURL: 'https://vydaje-843c0.firebaseio.com',
  // projectId: 'vydaje-843c0',
  // storageBucket: 'vydaje-843c0.appspot.com',
  // messagingSenderId: '823562082026'
  apiKey: 'AIzaSyDMhrKbQ_QcZwwENFfDE-2aPO0vUDQ-zZU',
  authDomain: 'vydaje-public.firebaseapp.com',
  databaseURL: 'https://vydaje-public.firebaseio.com',
  projectId: 'vydaje-public',
  storageBucket: 'vydaje-public.appspot.com',
  messagingSenderId: '675467381603'
};

const fire = firebase.initializeApp(config);

export default fire;
