import React, { Component } from 'react';
import { View, Text, NetInfo } from 'react-native';
import { Button } from 'native-base';
import { Font, AppLoading } from 'expo';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';
// import { persistStore, autoRehydrate } from 'redux-persist';
// import { AsyncStorage } from 'react-native';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import { requestTransactions } from './actions';
import App from './components/App';
import Reload from './components/Reload';

const store = createStore(rootReducer, compose(applyMiddleware(thunk) /* autoRehydrate() */));

// persistStore(store, { storage: AsyncStorage, whitelist: ['transactions'] });

/* TODO:delete in production */
console.ignoredYellowBox = ['Setting a timer'];

class index extends Component {
  state = {
    loaded: false,
    status: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      'dosis-regular': require('../assets/fonts/Dosis-Regular.ttf'),
      'dosis-bold': require('../assets/fonts/Dosis-Bold.ttf'),
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
    });
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    await NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({ status: isConnected });
      console.log('Now is: ' + (isConnected ? 'online' : 'offline'));
      if (isConnected) {
        store.dispatch(requestTransactions());
      }
    });
    this.setState({ loaded: true });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
  }

  handleConnectivityChange = isConnected => {
    if (isConnected) {
      store.dispatch(requestTransactions());
    }
    this.setState({ status: isConnected });
  };

  reload = () => {
    this.forceUpdate();
  };

  render() {
    const { loaded, status } = this.state;
    return (
      <Provider store={store}>
        {loaded ? status ? <App /> : <Reload onPress={() => this.reload} /> : <AppLoading />}
      </Provider>
    );
  }
}

export default index;
