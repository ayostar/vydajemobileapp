import React from 'react';
import { Text } from 'react-native';

export const categories = [
  { name: 'Jídlo/Nealko', icon: 'md-pizza' },
  { name: 'Alko', icon: 'md-beer' },
  { name: 'Restaurace', icon: 'md-restaurant' },
  { name: 'Byt', icon: 'md-home' },
  { name: 'Oblečení', icon: 'md-shirt' },
  { name: 'Cestování', icon: 'md-plane' },
  { name: 'Vzdělávání', icon: 'md-school' },
  { name: 'Sport', icon: 'md-bicycle' },
  { name: 'Půjčky', icon: 'md-cash' },
  { name: 'Ostatní', icon: 'md-help' }
];

export const types = ['výdaj', 'příjem'];

export function generateCategory(transactions, category) {
  return Object.keys(transactions)
    .filter(item => transactions[item].category === category)
    .map(item => {
      const { type, amount } = transactions[item];
      if (type === 'výdaj') {
        return `-${amount}`;
      }
      return amount;
    })
    .reduce((a, b) => {
      return +a + +b;
    }, 0);
}

export function generateBalanceDay(transactions, today) {
  return Object.keys(transactions)
    .filter(item => transactions[item].date.slice(0, 2) === today)
    .map(item => {
      const { type, amount } = transactions[item];
      if (type === 'výdaj') {
        return `-${amount}`;
      }
      return 0;
    })
    .reduce((a, b) => {
      return +a + +b;
    }, 0);
}

export function generateBalanceMonth(transactions) {
  return Object.keys(transactions)
    .map(item => {
      const { type, amount } = transactions[item];
      if (type === 'výdaj') {
        return `-${amount}`;
      }
      return amount;
    })
    .reduce((a, b) => {
      return +a + +b;
    }, 0);
}

export function colorAmount(num) {
  return <Text style={{ color: num < 0 ? '#f00' : '#0f0' }}>{num > 0 ? `+${num}` : num}</Text>;
}
export function colorTransactionAmount(num, type) {
  if (type === 'příjem') {
    return <Text style={{ color: '#0f0' }}>{`+${num}`}</Text>;
  }
  return <Text style={{ color: '#f00' }}>{`-${num}`}</Text>;
}
