import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

const baseWidth = 350;
const baseHeight = 680;

const scale = size => width / baseWidth * size;
const scaleVertical = size => height / baseHeight * size;

export { scale, scaleVertical };
