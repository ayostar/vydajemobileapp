import React from 'react';
import { View, Text } from 'react-native';

const TransactionsHeader = ({ title }) => {
  const { containerStyle, textStyle } = styles;
  return (
    <View style={containerStyle}>
      <Text style={textStyle}>{title}</Text>
    </View>
  );
};

const styles = {
  containerStyle: {
    padding: 5,
    marginLeft: 10,
    marginTop: 10,
    marginBottom: -5
  },
  textStyle: {
    fontFamily: 'dosis-bold',
    fontSize: 18
  }
};

export default TransactionsHeader;
