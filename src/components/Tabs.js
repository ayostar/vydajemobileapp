import React from 'react';
import { View } from 'react-native';
import { TabNavigator } from 'react-navigation';
import { Button, Text, Icon } from 'native-base';
import Categories from '../containers/Categories';
import Transactions from '../containers/Transactions';
import TabButton from './TabButton';

const Tabs = TabNavigator(
  {
    Categories: { screen: Categories },
    Transactions: { screen: Transactions }
  },
  {
    swipeEnabled: false,
    tabBarComponent: props => {
      const { index } = props.navigationState;
      const { tabsContainer } = styles;
      return (
        <View style={tabsContainer}>
          <View style={{ flex: 1 }}>
            <TabButton
              index={index}
              id={0}
              onPress={() => props.navigation.navigate('Categories')}
              title="Kategorie"
              icon="md-apps"
            />
          </View>
          <View style={{ flex: 1 }}>
            <TabButton
              index={index}
              id={1}
              onPress={() => props.navigation.navigate('Transactions')}
              title="Transakce"
              icon="md-swap"
            />
          </View>
        </View>
      );
    }
  }
);

const styles = {
  tabsContainer: {
    width: '100%',
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5
  }
};

export default Tabs;
