import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const Reload = ({ onPress }) => {
  const { reloadContainer, titleStyle, buttonStyle } = styles;
  return (
    <View style={reloadContainer}>
      <Text style={titleStyle}>No internet connection!</Text>
      <TouchableOpacity style={buttonStyle} onPress={onPress}>
        <Text style={{ fontFamily: 'dosis-regular' }}>Reload connection</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  reloadContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleStyle: {
    fontFamily: 'dosis-bold',
    fontSize: 22,
    marginBottom: 5
  },
  buttonStyle: {
    padding: 10,
    borderWidth: 0.5,
    borderColor: '#000'
  }
};

export default Reload;
