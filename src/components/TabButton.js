import React from 'react';
import { Button, Icon, Text } from 'native-base';

const TabButton = ({ index, id, onPress, title, icon }) => {
  const { textStyle } = styles;
  return (
    <Button
      block
      style={{ backgroundColor: index === id ? '#00D0B1' : '#fff' }}
      active={index === id}
      onPress={onPress}
    >
      <Icon name={icon} style={{ color: index === id ? '#fff' : '#00D0B1' }} />
      <Text style={{ ...textStyle, color: index === id ? '#fff' : '#00D0B1' }}>{title}</Text>
    </Button>
  );
};

const styles = {
  textStyle: {
    paddingLeft: 0,
    fontFamily: 'dosis-bold',
    fontSize: 16
  }
};

export default TabButton;
