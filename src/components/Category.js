import React from 'react';
import { ListItem, Text, Icon, Left, Body, Right } from 'native-base';
import { colorAmount } from '../utils';

const Category = ({ icon, title, value }) => {
  const { textStyle, iconStyle } = styles;
  return (
    <ListItem>
      <Left>
        <Icon style={iconStyle} name={icon} />
        <Text style={textStyle}>{title}</Text>
      </Left>
      <Right>
        <Text style={textStyle}>{colorAmount(value)}</Text>
      </Right>
    </ListItem>
  );
};

const styles = {
  textStyle: {
    fontFamily: 'dosis-bold',
    fontSize: 22
  },
  iconStyle: {
    fontSize: 22
  }
};

export default Category;
