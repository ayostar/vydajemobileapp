import React from 'react';
import { View } from 'react-native';
import { Text } from 'native-base';

const AddTitle = () => {
  const { logoContainer, titleStyle } = styles;
  return (
    <View style={logoContainer}>
      <Text style={titleStyle}>Přidat položku</Text>
    </View>
  );
};

const styles = {
  logoContainer: {
    flexDirection: 'row',
    marginLeft: 20
  },
  titleStyle: {
    fontSize: 24,
    fontFamily: 'dosis-bold'
  }
};

export default AddTitle;
