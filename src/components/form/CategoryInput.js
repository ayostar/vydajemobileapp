import React from 'react';
import { Text, Picker } from 'react-native';
import { Item } from 'native-base';

const CategoryInput = ({ title, selectedValue, onValueChange, categories }) => {
  const { inputContainerStyle, inputLabelStyle } = styles;
  return (
    <Item style={inputContainerStyle}>
      <Text style={inputLabelStyle}>Zvol kategorii:</Text>
      <Picker
        mode="dropdown"
        style={{ width: 140 }}
        selectedValue={selectedValue}
        onValueChange={onValueChange}>
        {categories.map((item, index) => {
          const { name } = item;
          return <Picker.Item key={index} label={name} value={name} />;
        })}
      </Picker>
    </Item>
  );
};

const styles = {
  inputContainerStyle: {
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    marginLeft: 0,
    marginRight: 10,
    marginBottom: 15
  },
  inputLabelStyle: {
    fontFamily: 'dosis-regular',
    fontSize: 22,
    marginLeft: 10,
    marginRight: 10
  }
};

export default CategoryInput;
