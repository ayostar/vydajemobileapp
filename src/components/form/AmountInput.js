import React from 'react';
import { Text, TextInput } from 'react-native';
import { Item } from 'native-base';

const AmountInput = ({ title, value, onFocus, onBlur, onChangeText }) => {
  const { inputContainerStyle, inputLabelStyle, inputStyle } = styles;
  return (
    <Item style={inputContainerStyle}>
      <Text style={inputLabelStyle}>{title}</Text>
      <TextInput
        style={inputStyle}
        placeholder="0"
        value={value}
        keyboardType="numeric"
        onFocus={onFocus}
        onBlur={onBlur}
        onChangeText={onChangeText}
      />
    </Item>
  );
};

const styles = {
  inputContainerStyle: {
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRightWidth: 1,
    marginLeft: 0,
    marginRight: 10,
    marginBottom: 15
  },
  inputLabelStyle: {
    fontFamily: 'dosis-regular',
    fontSize: 22,
    marginLeft: 10,
    marginRight: 10
  },
  inputStyle: {
    textAlign: 'right',
    fontFamily: 'dosis-regular',
    width: 90,
    padding: 10,
    fontSize: 22
  }
};

export default AmountInput;
