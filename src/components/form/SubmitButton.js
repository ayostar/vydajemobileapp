import React from 'react';
import { View, Text } from 'react-native';
import { Button } from 'native-base';

const SubmitButton = ({ onKeyboard, onPress, title }) => {
  const { buttonContainerStyle, buttonStyle, buttonTextStyle } = styles;
  return (
    <View style={{ ...buttonContainerStyle, bottom: onKeyboard ? -70 : 5 }}>
      <Button block large style={buttonStyle} onPress={onPress}>
        <Text style={buttonTextStyle}>PŘIDAT</Text>
      </Button>
    </View>
  );
};

const styles = {
  buttonContainerStyle: {
    position: 'absolute',
    width: '100%'
  },
  buttonStyle: {
    backgroundColor: '#00D0B1'
  },
  buttonTextStyle: {
    color: '#fff',
    fontSize: 22,
    fontFamily: 'dosis-bold'
  }
};

export default SubmitButton;
