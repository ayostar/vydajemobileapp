import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

const DatePickerButton = ({ title, onPress }) => {
  const { textStyle } = styles;
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={textStyle}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    color: '#00D0B1',
    fontFamily: 'dosis-regular'
  }
};

export default DatePickerButton;
