import React from 'react';
import { StackNavigator } from 'react-navigation';
import AuthScreen from '../screens/AuthScreen';
import HomeScreen from '../screens/HomeScreen';
import AddScreen from '../screens/AddScreen';

const App = StackNavigator(
  {
    // Auth: { screen: AuthScreen },
    Home: { screen: HomeScreen },
    Add: { screen: AddScreen }
  },
  {
    navigationOptions: {
      headerStyle: {
        marginTop: -24,
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgba(0, 0, 0, 0.2)',
        elevation: 0,
        shadowOpacity: 0
      }
    },
    lazy: true
  }
);

export default App;
