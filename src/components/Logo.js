import React from 'react';
import { View, Image } from 'react-native';
import { Text } from 'native-base';

const Logo = () => {
  const { logoContainer, imageStyle, titleStyle } = styles;
  return (
    <View style={logoContainer}>
      <Image source={require('../../assets/images/vydaje-logo-outlined.png')} style={imageStyle} />
      <Text style={titleStyle}>Výdaje App</Text>
    </View>
  );
};

const styles = {
  logoContainer: {
    flexDirection: 'row',
    marginLeft: 20
  },
  imageStyle: {
    width: 30,
    height: 30,
    marginRight: 10
  },
  titleStyle: {
    fontSize: 24,
    fontFamily: 'dosis-bold'
  }
};

export default Logo;
