import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Text } from 'native-base';

const AddTabButton = ({ index, id, title, onPress }) => {
  const { buttonStyle, buttonActive, textStyle, textActive } = styles;

  return (
    <TouchableOpacity
      style={index === id ? { ...buttonStyle, ...buttonActive } : buttonStyle}
      onPress={onPress}
    >
      <Text style={index === id ? { ...textStyle, ...textActive } : textStyle}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    flex: 1,
    marginRight: 5,
    height: 80,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    borderWidth: 1
  },
  buttonActive: {
    backgroundColor: '#00D0B1',
    borderColor: '#00D0B1'
  },
  textStyle: {
    color: '#000',
    fontFamily: 'dosis-bold',
    fontSize: 22,
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  textActive: {
    color: '#fff'
  }
};

export default AddTabButton;
