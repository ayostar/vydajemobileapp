import React, { Component } from 'react';
import { connect } from 'react-redux';
import { generateBalanceDay, generateBalanceMonth, colorAmount } from '../utils';
import { View, Text, Image } from 'react-native';
import { Card, CardItem } from 'native-base';
import moment from 'moment';
import localization from 'moment/locale/cs';
import _ from 'lodash';

class Boxes extends Component {
  render() {
    const { transactions } = this.props;
    const { boxesContainer, title, subtitle, hr, amountContainer, amount } = styles;
    const today = moment().format('DD');
    const balanceDay = generateBalanceDay(transactions || {}, today);
    const balanceMonth = generateBalanceMonth(transactions || {});

    return (
      <View style={boxesContainer}>
        <Card>
          <CardItem style={{ flexDirection: 'column' }}>
            <Text style={title}>
              {_.capitalize(
                moment()
                  .locale('cs', localization)
                  .format('dddd')
              )}
            </Text>
            <Text style={subtitle}>{moment().format('DD.MM', { trim: true })}</Text>
            <View style={hr} />
            <View style={amountContainer}>
              <Text style={amount}>{colorAmount(balanceDay)}</Text>
              <Image
                style={{ width: 32, height: 32 }}
                source={require('../../assets/images/mince.png')}
              />
            </View>
          </CardItem>
        </Card>
        <Card>
          <CardItem style={{ flexDirection: 'column' }}>
            <Text style={title}>
              {_.capitalize(
                moment()
                  .locale('cs', localization)
                  .format('MMMM')
              )}
            </Text>
            <Text style={subtitle}>{moment().format('YYYY')}</Text>
            <View style={hr} />
            <View style={amountContainer}>
              <Text style={amount}>{colorAmount(balanceMonth)}</Text>
              <Image
                style={{ width: 32, height: 32 }}
                source={require('../../assets/images/mince.png')}
              />
            </View>
          </CardItem>
        </Card>
      </View>
    );
  }
}

const styles = {
  boxesContainer: {
    flexDirection: 'row',
    marginBottom: 10
  },
  title: {
    fontSize: 28,
    textAlign: 'center',
    fontFamily: 'dosis-bold'
  },
  subtitle: {
    fontSize: 13,
    textAlign: 'center',
    fontFamily: 'dosis-regular'
  },
  hr: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)',
    marginTop: 10,
    marginBottom: 10
  },
  amountContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  amount: {
    fontSize: 32,
    fontFamily: 'dosis-bold'
  }
};

function mapStateToProps({ vydaje }) {
  return { transactions: vydaje.transactions };
}

export default connect(mapStateToProps)(Boxes);
