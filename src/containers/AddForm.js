import React, { Component } from 'react';
import { KeyboardAvoidingView, DatePickerAndroid, View } from 'react-native';
import { connect } from 'react-redux';
import { setTransaction } from '../actions';
import { types, categories } from '../utils';
import moment from 'moment';
import AddTabButton from '../components/AddTabButton';
import CategoryInput from '../components/form/CategoryInput';
import AmountInput from '../components/form/AmountInput';
import DescInput from '../components/form/DescInput';
import SubmitButton from '../components/form/SubmitButton';
import DatePickerButton from '../components/form/DatePickerButton';

class AddForm extends Component {
  state = {
    date: moment().format('DD.MM.YYYY'),
    type: types[0],
    amount: '0',
    category: categories[0].name,
    desc: '',
    alkoMix: '0',
    jidloMix: '0',
    tabIndex: 0,
    onKeyboard: false
  };

  handleTabToggle(num) {
    const { tabIndex } = this.state;
    if (num !== tabIndex) {
      this.setState({
        tabIndex: num,
        type: num === 2 ? types[0] : types[num]
      });
    }
  }

  /* if true -> handles Amount, if false -> handles Description */
  handleFocus(type) {
    if (type) {
      this.setState({ [type]: '', onKeyboard: true });
    }
    this.setState({ onKeyboard: true });
  }

  handleBlur() {
    this.setState({ onKeyboard: false });
  }

  handleChange(val, type) {
    this.setState({ [type]: val });
  }

  handleSubmit() {
    const { date, type, category, amount, desc, alkoMix, jidloMix, tabIndex } = this.state;
    const { setTransaction } = this.props;

    /* TODO: add form validation */
    if (tabIndex === 2) {
      setTransaction({
        date,
        type: types[0],
        category: categories[1].name,
        amount: +alkoMix,
        desc
      });
      setTransaction({
        date,
        type: types[0],
        category: categories[0].name,
        amount: +jidloMix,
        desc
      });
    } else {
      setTransaction({
        date,
        type,
        category: type === 'příjem' ? null : category,
        amount: +amount,
        desc
      });
    }
    this.props.navigation.goBack();
  }

  async handleDatePicker() {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date(),
        mode: 'default'
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        let monthWithLeadingZero = month + 1 < 10 ? `0${month + 1}` : month + 1;
        let dayWithLeadingZero = day < 10 ? `0${day}` : day;
        this.setState({ date: `${dayWithLeadingZero}.${monthWithLeadingZero}.${year}` });
      }
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  }

  render() {
    const { date, amount, desc, category, alkoMix, jidloMix, tabIndex, onKeyboard } = this.state;
    const { addFormContainerStyle, tabContainerStyle } = styles;

    return (
      <KeyboardAvoidingView style={addFormContainerStyle}>
        <View style={tabContainerStyle}>
          <AddTabButton
            index={tabIndex}
            id={0}
            title="Výdej"
            onPress={() => this.handleTabToggle(0)}
          />
          <AddTabButton
            index={tabIndex}
            id={1}
            title="Příjem"
            onPress={() => this.handleTabToggle(1)}
          />
          <AddTabButton
            index={tabIndex}
            id={2}
            title="Mix"
            onPress={() => this.handleTabToggle(2)}
          />
        </View>

        {tabIndex === 0 && (
          <CategoryInput
            title="Zvol kategorii:"
            selectedValue={category}
            onValueChange={val => this.handleChange(val, 'category')}
            categories={categories}
          />
        )}

        {(tabIndex === 0 || tabIndex === 1) && (
          <AmountInput
            title="Částka:"
            value={amount}
            onFocus={() => this.handleFocus('amount')}
            onBlur={() => this.handleBlur()}
            onChangeText={val => this.handleChange(val, 'amount')}
          />
        )}

        {tabIndex === 2 && [
          <AmountInput
            key={0}
            title="AlkoMix"
            value={alkoMix}
            onFocus={() => this.handleFocus('alkoMix')}
            onBlur={() => () => this.handleBlur()}
            onChangeText={val => this.handleChange(val, 'alkoMix')}
          />,
          <AmountInput
            key={1}
            title="JídloMix"
            value={jidloMix}
            onFocus={() => this.handleFocus('jidloMix')}
            onBlur={() => () => this.handleBlur()}
            onChangeText={val => this.handleChange(val, 'jidloMix')}
          />
        ]}

        <DescInput
          title="Popis"
          value={desc}
          onFocus={() => this.handleFocus()}
          onBlur={() => this.handleBlur()}
          onChangeText={val => this.handleChange(val, 'desc')}
        />

        <DatePickerButton title={`Změň datum... ${date}`} onPress={() => this.handleDatePicker()} />

        <SubmitButton onKeyboard={onKeyboard} onPress={() => this.handleSubmit()} title="PŘIDEJ" />
      </KeyboardAvoidingView>
    );
  }
}

const styles = {
  addFormContainerStyle: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#fff'
  },
  tabContainerStyle: {
    flexDirection: 'row',
    marginBottom: 10
  }
};

export default connect(null, { setTransaction })(AddForm);
