import React, { Component } from 'react';
import { View, TouchableHighlight } from 'react-native';
import { Button, Icon, Text, ListItem, Badge } from 'native-base';
import { deleteTransaction } from '../actions';
import { connect } from 'react-redux';
import { colorTransactionAmount } from '../utils';
import moment from 'moment';

import Swipeable from 'react-native-swipeable';

class Transaction extends Component {
  render() {
    const {
      transactionContainer,
      amountStyle,
      badgeContainerStyle,
      badgeTextStyle,
      descStyle
    } = styles;
    const { id, type, category, amount, date, desc } = this.props;

    const year = date && date.slice(6, 10);
    const month = date && date.slice(3, 5);
    const day = date && date.slice(0, 2);

    const icon =
      type !== 'příjem' ? (
        <Icon name="md-arrow-round-down" style={{ color: '#f00' }} />
      ) : (
        <Icon name="md-arrow-round-up" style={{ color: '#0f0' }} />
      );

    const rightButtons = [
      <Button
        transparent
        style={{ flex: 1 }}
        onPress={() => this.props.deleteTransaction(id, { year, month })}
      >
        <Icon name="md-close-circle" style={{ color: '#f00', fontSize: 32 }} />
      </Button>
    ];

    return (
      <Swipeable rightButtons={rightButtons}>
        <ListItem>
          <View style={{ flexDirection: 'row' }}>
            {icon}
            <Text style={amountStyle}>{colorTransactionAmount(amount, type)}</Text>
            <Badge style={badgeContainerStyle}>
              <Text style={badgeTextStyle}>{category}</Text>
            </Badge>
            <Text style={descStyle}>{desc}</Text>
          </View>
        </ListItem>
      </Swipeable>
    );
  }
}

const styles = {
  transactionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    padding: 5,
    marginBottom: 5,
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.2)'
  },
  amountStyle: {
    fontSize: 22,
    marginLeft: 10,
    width: 80,
    fontFamily: 'dosis-bold'
  },
  badgeContainerStyle: {
    backgroundColor: '#00D0B1'
  },
  badgeTextStyle: {
    fontFamily: 'dosis-bold',
    fontSize: 16
  },
  descStyle: {
    fontFamily: 'dosis-regular',
    flex: 1,
    textAlign: 'right'
  }
};

export default connect(null, { deleteTransaction })(Transaction);
