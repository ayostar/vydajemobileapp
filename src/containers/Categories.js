import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { Text, List } from 'native-base';
import { connect } from 'react-redux';
import { categories, generateCategory } from '../utils';
import Category from '../components/Category';

class Categories extends Component {
  render() {
    const { transactions } = this.props;
    const values = categories.map((item, index) => {
      return generateCategory(transactions || {}, categories[index].name);
    });

    return (
      <ScrollView>
        <List>
          {categories.map((item, index) => {
            const { icon, name } = item;
            return <Category key={index} icon={icon} title={name} value={values[index]} />;
          })}
        </List>
      </ScrollView>
    );
  }
}

function mapStateToProps({ vydaje }) {
  return {
    transactions: vydaje.transactions
  };
}

export default connect(mapStateToProps)(Categories);
