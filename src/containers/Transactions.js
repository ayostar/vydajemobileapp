import React, { Component } from 'react';
import { View, ScrollView, Text, FlatList, SectionList } from 'react-native';
import { List, ListItem } from 'native-base';
import { connect } from 'react-redux';
import Transaction from './Transaction';
import TransactionsHeader from '../components/TransactionsHeader';
import _ from 'lodash';
import moment from 'moment';
import localization from 'moment/locale/cs';

class Transactions extends Component {
  render() {
    const { transactions } = this.props;

    const dateRest = `${moment().format('MM')}.${moment().format('YYYY')}`;

    const _arr = _.map(transactions, (item, index) => {
      const { type, category, amount, date, desc } = item;
      return { id: index, key: index, type, category, amount, date, desc };
    });

    let result = [];

    for (let i = 1; i < 32; i++) {
      const iterator = i < 10 ? `0${i}` : `${i}`;
      const dayArray = _arr.filter(item => item.date.slice(0, 2) === iterator);
      const dayVerbal = moment(`${iterator}.${dateRest}`, 'DD.MM.YYYY')
        .locale('cs', localization)
        .format('dddd');
      const title = `${dayVerbal} - ${iterator}.${dateRest}`;
      if (dayArray.length > 0) {
        const _day = { data: dayArray, title };
        result.push(_day);
      }
    }

    return (
      <ScrollView>
        <List style={{ flex: 1 }}>
          <SectionList
            sections={result.reverse()}
            renderSectionHeader={({ section }) => (
              <TransactionsHeader title={_.capitalize(section.title)} />
            )}
            renderItem={({ item }) => {
              const { id, type, category, amount, date, desc } = item;
              return (
                <Transaction
                  id={id}
                  type={type}
                  category={category}
                  amount={amount}
                  date={date}
                  desc={desc}
                />
              );
            }}
          />
        </List>
      </ScrollView>
    );
  }
}

function mapStateToProps({ vydaje }) {
  return { transactions: vydaje.transactions };
}

export default connect(mapStateToProps)(Transactions);
