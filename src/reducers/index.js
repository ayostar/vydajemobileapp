import { combineReducers } from 'redux';
import authReducer from './auth-reducer';
import transactionsReducer from './transactions-reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  vydaje: transactionsReducer
});

export default rootReducer;
