// import { REHYDRATE } from 'redux-persist/constants';

const initialState = {
  transactions: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    // case REHYDRATE:
    //   return action.payload.transactions || [];
    case 'FETCH_TRANSACTIONS': {
      return { ...state, transactions: action.payload };
    }
    default: {
      return state;
    }
  }
}
