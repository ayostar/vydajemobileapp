import React, { Component } from 'react';
import { View } from 'react-native';
import { Icon, Text, Button } from 'native-base';
import Boxes from '../containers/Boxes';
import Logo from '../components/Logo';
import Tabs from '../components/Tabs';

class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: <Logo />,
    headerRight: (
      <View style={styles.headerRightContainer}>
        <Button transparent style={styles.buttonStyle} onPress={() => navigation.navigate('Add')}>
          <Icon name="md-add-circle" style={styles.iconStyle} />
        </Button>
      </View>
    )
  });

  render() {
    const { appContainer } = styles;
    return (
      <View style={appContainer}>
        <Boxes />
        <Tabs />
      </View>
    );
  }
}

const styles = {
  appContainer: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerRightContainer: {
    marginRight: 10
  },
  buttonStyle: {
    borderColor: '#00D0B1'
  },
  iconStyle: {
    color: '#00D0B1',
    fontSize: 32
  }
};

export default HomeScreen;
