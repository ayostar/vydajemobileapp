import React, { Component } from 'react';
import { View } from 'react-native';
import { Text } from 'native-base';
import { connect } from 'react-redux';
import { facebookLogin } from '../actions';

class AuthScreen extends Component {
  componentDidMount() {
    this.props.facebookLogin();
  }

  render() {
    return (
      <View>
        <Text>This is Auth Screen</Text>
      </View>
    );
  }
}

export default connect(null, { facebookLogin })(AuthScreen);
