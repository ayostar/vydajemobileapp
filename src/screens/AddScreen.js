import React, { Component } from 'react';
import { View } from 'react-native';
import AddForm from '../containers/AddForm';
import AddTitle from '../components/AddTitle';

class AddScreen extends Component {
  static navigationOptions = {
    headerTitle: <AddTitle />
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <AddForm navigation={this.props.navigation} />
      </View>
    );
  }
}

export default AddScreen;
